package com.cdc;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by r0h17 on 1/31/15.
 */
public class Chapter1 {

    public static void main(String[] args) {
        new Chapter1().problem4();
    }

    private void problem4() {
        String input1 = new String("Tom Cruise");
        String input2 = new String("So I'm cuter");
        Map<String, Integer> wordCountMap = new HashMap<>();
        int input1Total = 0;
        int input2Total = 0;
        input1Total = getTotalAnsci(input1.trim().toUpperCase());
        input2Total = getTotalAnsci(input2.trim().toUpperCase());
        if(input1Total == input2Total) System.out.println("Yes anagram");
        else System.out.println("not anagram");

    }

    private int getTotalAnsci(String input1) {
        int total = 0;
        for(Character c: input1.toCharArray()){
            if(Character.isUpperCase(c))
            total += ((int) c);
        }
        return total;
    }


    private void problem5() {
        String inputString = new String("This is a interview  string");
        StringTokenizer st = new StringTokenizer(inputString);
        StringBuilder sb = new StringBuilder();
        while (st.hasMoreElements()){
            //System.out.println(st.nextElement());
            sb.append(st.nextElement()+(st.hasMoreElements()?"%20":""));
        }
        System.out.println(sb.toString());
    }

    private class MyString {
        private final String input;

        public MyString(String s) {
            this.input = s;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }
}
