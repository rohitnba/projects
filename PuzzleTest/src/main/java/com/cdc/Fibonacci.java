package com.cdc;

/**
 * Created by r0h17 on 1/29/15.
 */
public class Fibonacci {

    public static void main(String[] args) {

        int limit = 15;
        for(int i=1; i<=limit; i++){
            System.out.print(fib(i) +" ");
        }
    }

    public static int fib(int n){
        if(n==1 || n==2) return 1;

        return fib(n-1)+fib(n-2);

    }
}
