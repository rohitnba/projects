package com.codejam.fbhc;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by r0h17 on 1/9/15.
 */
public class NewYearNew {

    public static void main(String[] args) throws FileNotFoundException {
        Integer[] test = new Integer[]{1,3,4};
        Scanner sc = new Scanner(new FileReader("resources/newyear.txt"));
        PrintWriter pw = new PrintWriter(System.out);

        new NewYearNew().solve(sc, pw);

        pw.flush();
        pw.close();
        sc.close();
    }

    private void solve(Scanner sc, PrintWriter pw) {
        int t = sc.nextInt();
        int gp = sc.nextInt(), gc = sc.nextInt(), gf = sc.nextInt();
        int totalFood = sc.nextInt();
        /*Integer[] p = new Integer[totalFood];
        Integer[] c = new Integer[totalFood];
        Integer[] f = new Integer[totalFood];*/

        List<Food> foods = new ArrayList<Food>();
        for (int i = 0; i < totalFood; i++) {
            foods.add(new Food(sc.nextInt(),sc.nextInt(),sc.nextInt()));
        }

        List<ArrayList<Integer>> pPairs = getPair(foods, gp);
        for(ArrayList<Integer> temp: pPairs){
            for(Integer tempIn: temp){
                System.out.printf(tempIn+",");
            }
            System.out.println();
        }


        System.out.println("");
    }

    //   System.out.println(testRecursive(5));
    public static String testRecursive(int times){
        String output = "";
        times--;
        if(times !=0) output = testRecursive(times);
        return output+times;
    }

    public static List<ArrayList<Integer>> getPair(List<Food> foods, int k) {
        HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
        String result = "";
        List<String> strings = new ArrayList<String>();
        /**
         * First store array elements into hashmap with key as the value of the
         * array This has time complexity of O(n) and space complexity of O(n)
         */
        for (Food food: foods) {
            hm.put(food.getP(), hm.containsKey(food.getP())?hm.get(food.getP())+1:1);
        }

        List<ArrayList<Integer>> resultList = new ArrayList<ArrayList<Integer>>();
        for(Integer keyValue: hm.keySet()){
            Map<Integer,Integer> clone = (Map<Integer, Integer>) hm.clone();
            clone.put(keyValue, clone.get(keyValue) - 1);
            add(keyValue + lookupsum(clone, k - keyValue), resultList);
        }


        return resultList;
    }

    private static void add(String s, List<ArrayList<Integer>> resultList) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(String splitValue: s.split(",")){
            if(!splitValue.isEmpty()){
                result.add(Integer.parseInt(splitValue));
            }
        }
        resultList.add(result);
    }

    private static String lookupsum(Map<Integer, Integer> hm, int k) {
        String output = "";
        if(k==0) return "";
        if(k<0) return "NULL";
        for(Integer keyValue: hm.keySet()){
            if(keyValue<=k && hm.get(keyValue)>0) {
                output+=","+keyValue;
                hm.put(keyValue, hm.get(keyValue) - 1);
                output += ","+lookupsum(hm, k-keyValue);
            }
            if(stringsum(output)==k){
                break;
            }else{
                output="";
            }
        }

        return output;
    }

    private static int stringsum(String output) {
        int sum = 0;
        for(String c:output.split(",")){
            if(!c.isEmpty())
                sum+=Integer.parseInt(""+c);
        }
        return sum;
    }

    public class Food{
        int p;
        int c;
        int f;

        public Food(int p, int c, int f) {
            this.p = p;
            this.c = c;
            this.f = f;
        }

        public int getP() {
            return p;
        }

        public int getC() {
            return c;
        }

        public int getF() {
            return f;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }

}