package com.httpproxy;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by r0h17 on 1/25/15.
 */
public class TestHttpProxy {

    public static void main(String[] args) throws IOException {

        String proxyIpAddress = "180.250.44.43";
        System.setProperty("https.proxyHost", proxyIpAddress);
        System.setProperty("http.proxyHost", proxyIpAddress);
        System.setProperty("https.proxyPort", "80");
        System.setProperty("http.proxyPort", "80");

        Document doc = Jsoup.connect("https://www.google.com/search?q=what+is+my+ip")
                .header("Accept-Encoding", "gzip, deflate")
                .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko)" +
                        " Chrome/39.0.2171.99 Safari/537.36")
                .maxBodySize(0)
                .timeout(600000)
                .get();

        System.out.println("proxy site ==>>"+doc.body().text().contains(proxyIpAddress));
        System.out.println("same site ==>>"+doc.body().text().contains("107.185.199.3"));

        System.out.println(doc.body().text());
    }

}
