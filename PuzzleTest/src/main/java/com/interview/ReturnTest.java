package com.interview;

/**
 * Created by r0h17 on 1/30/15.
 */
public class ReturnTest {
    public static void main(String[] args) {
        System.out.println(new ReturnTest().test());
    }

    private String test() {
        try{
            return "0";
        }
        finally {
            System.out.println("before returning 0");
        }
    }
}
