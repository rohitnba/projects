package com.interview;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by r0h17 on 1/22/15.
 */
public class Sample extends Thread{

    public Sample(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        /*String interview = "Hoieiruwoeiuroiweuroiwueoiruweor";
        String output = "";
        for (int i = 0; i < 1000000000; i++) {
                output=output+interview;
        }
        System.out.println(output);*/

            ExecutorService executorService = Executors.newFixedThreadPool(5);
        CompletionService<Integer> completionService = new ExecutorCompletionService<Integer>(executorService);
            Future<Integer>[] futures = new Future[5];
            for (int i = 0; i < futures.length; i++) {
                completionService.submit(new MyCallable());
            }
            for (int i = 0; i < futures.length; i++) {
                Integer retVal = completionService.take().get();
                System.out.println(retVal);
            }
            executorService.shutdown();
    }

    static AtomicInteger temp = new AtomicInteger(0);
    private static class MyCallable implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            Thread.sleep(1000);
            return temp.incrementAndGet();
        }

    }
}
