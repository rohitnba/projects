package com.interview.UHG;

import java.io.*;
import java.util.*;

public class Sort {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader("resources/uhg.txt"));
        Comparator<String> stringComparator = new Comparator<String>() {
            @Override
            public int compare(String s, String s2) {

                return s2.compareTo(s);
            }
        };
        Map<String, String> map=new TreeMap<String, String>(stringComparator);
        String line="";
        while((line=reader.readLine())!=null){
            map.put(getField(line),line);
        }
        reader.close();
        PrintWriter writer = new PrintWriter(System.out);
        for(String val : map.values()){
        	writer.write(val);	
        	writer.write('\n');
        }
        writer.close();
    }

    private static String getField(String line) {
    	return line.split(" ")[0];//extract value you want to sort on
    }
}