package com.interview.bluetalon;

import java.util.*;

/**
 * Created by r0h17 on 2/12/15.
 */
public class SortAndUnify {
    public static void main(String[] args) {
        List<Integer> intValue = new ArrayList<Integer>(){{
            add(5);
            add(3);
            add(8);
            add(1);
            add(-1);
            add(51);
            add(12);
            add(15);
            //-1,1,3,5,8,12,15,51
            //rahulshrivastava@bluetalon.com
        }
        };



        int k = 5;

        System.out.println(intValue.get(k-1));

        for (int i = 0; i < intValue.size(); i++) {

        }

        /*SortAndUnify sortAndUnify = new SortAndUnify();
        for(Integer individualValue: sortAndUnify.sortAndMerge(intValue)){
            System.out.println(individualValue);
        }*/

    }


    private List<Integer> sortAndMerge(List<Integer> inputValues){

        Set<Integer> individualValue  = new HashSet<Integer>();
        for (Integer intValue: inputValues){
            individualValue.add(intValue);
        }

        List<Integer> output = new ArrayList<Integer>();
        output.addAll(individualValue);

        //1,2,-1,5,10,15
        Collections.sort(output);

        return output;
    }

}
