package com.interview.gumgum;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

/**
 * Created by r0h17 on 1/23/15.
 */
public class PageTagger {

    private MaxentTagger tagger;

    public MaxentTagger getTagger() {
        return tagger;
    }

    public PageTagger(){
        tagger = new MaxentTagger("resources/english-left3words-distsim.tagger");
    }

    public String tagText(String input){
        return tagger!=null? tagger.tagString(input): "";
    }

    public String getText(URL url) throws IOException {
        StringBuilder webPageString = new StringBuilder();
        Document doc = Jsoup.connect(url.toString())
                .header("Accept-Encoding", "gzip, deflate")
                .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko)" +
                        " Chrome/39.0.2171.99 Safari/537.36")
                .maxBodySize(0)
                .timeout(600000)
                .get();
        webPageString.append(doc.body().text());
        return doc.body().text();
    }

    public void process(URL url, PrintWriter printWriter){

        try {

            //Download the webpage into a string object
            String webPageText = getText(url);
            System.out.printf("Total Time for Downloading the text : ");

            //Preparing the file to save the output


            String[] strings = webPageText.split(" ");
            for (String string: strings){
                //System.out.println(tagText(string));
                printWriter.write(tagText(string) + "\n");
            }
            //Creating a parallel stream of the webpage string by creating alist of it and processing them in parallel
            /*Arrays.asList(webPageText.toString().split("\\s")).parallelStream()
                    .filter(s -> s!=null && !s.trim().isEmpty())
                    .forEach(s -> {
                        printWriter.write(tagText(s) + "\n");
                    });*/
            printWriter.flush();

            //Printing Stats

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws IOException {

        if(args.length<1) {
            System.out.println("Please enter a valid url string");
            System.exit(0);
        }
        PageTagger pageTagger = new PageTagger();
        PrintWriter printWriter = new PrintWriter("resources/output_gumgum.txt");
        for(String s: args){
            pageTagger.process(new URL(s), printWriter);
        }
    }

}
