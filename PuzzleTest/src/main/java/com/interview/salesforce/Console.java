package com.interview.salesforce;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by r0h17 on 1/28/15.
 */
public class Console {

    public static void main(String[] args) throws IOException {
        Scanner inputScanner = new Scanner(System.in);

        File file = new File(".");
        while(true){
            System.out.print(">> ");
            String inputString = inputScanner.nextLine();

            if(!inputString.trim().isEmpty()){

                String[] commandInputs = inputString.trim().split("\\s");
                switch (Command.fromString(commandInputs[0])){
                    case cd:
                        String newFilePath = file.getCanonicalPath().substring(0,file.getCanonicalPath().lastIndexOf("/"));
                        if(newFilePath.isEmpty())
                            file = new File("/");
                        else file = new File(newFilePath);
                        //file = new File();
                        System.out.println(file.getCanonicalPath());
                        break;
                    case ls:
                        for (File file1 : file.listFiles()) {
                            System.out.println(file1.getName());
                        }
                        break;
                    case pwd:
                        System.out.println(file.getCanonicalPath());
                        break;
                    case clear:
                        char esc = 27;
                        String clear = esc + "[2J";
                        System.out.print(clear);
                        break;
                    case exit:
                        System.exit(0);
                    default:
                        System.out.println("Command does not exist");
                }

            }

        }

    }

    public enum Command{
        cd,ls,pwd,clear,exit,novalue;

        public static Command fromString(String Str)
        {
            try {return valueOf(Str);}
            catch (Exception ex){return novalue;}
        }

    }
}
