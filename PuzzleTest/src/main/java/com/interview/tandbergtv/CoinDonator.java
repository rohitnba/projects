package com.interview.tandbergtv;

import java.util.Iterator;
import java.util.List;

import com.interview.tandbergtv.coinsort.coins.Coin;

/**
 * Class that would represent the people that would donate coins
 *
 */
public class CoinDonator implements Runnable {
	private List<Coin> bagOfCoins;
	private int delay;
	private String name;
	private CoinSortMachine coinSortMachine;
	
	public CoinDonator(String name, List<Coin> bagOfCoins, int delay, CoinSortMachine coinSortMachine) {
		this.bagOfCoins = bagOfCoins;	// the bag of coins this person has
		this.name = name;	// person's name
		this.delay = delay;	// delay between donations (in seconds)
		this.coinSortMachine = coinSortMachine; // the hopper of the coin sorter
	}
	
	public void run() {
		try {
			while(true) {
				Coin coin = donate();
				if (coinSortMachine.isHopperFull()) {
					System.out.printf("%s tries to donate a %s, but the hopper is full!\n", name, coin);
					bagOfCoins.add(coin);
				} else {
					System.out.printf("%s donates a %s   (hopper count: %d)\n", name, coin, coinSortMachine.currentHopperSize() + 1);
					coinSortMachine.addCoinToHopper(coin);
				}
				Thread.sleep(delay * 1000);
			}
		} catch (Exception e) {
			System.out.printf("***Donator '" + name + "' has no more coins to give.\n");
		}
	}
	
	public Coin donate() {
		Iterator<Coin> it = bagOfCoins.iterator();
		Coin ret = it.next();
		it.remove();
		return ret;
	}
}
