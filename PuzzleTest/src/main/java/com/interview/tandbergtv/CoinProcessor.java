package com.interview.tandbergtv;

/**
 * Created by r0h17 on 1/11/15.
 */
public interface CoinProcessor extends Runnable {
    @Override
    void run();

    void init();

    void clear();
}
