package com.interview.tandbergtv;

import com.interview.tandbergtv.coinsort.coins.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by r0h17 on 1/11/15.
 */
public class CoinProcessorImpl implements CoinProcessor, Runnable{

    BlockingQueue<Coin> hopper;

    List<CoinWrapper<Dime>> dimeWrapperContainer = new ArrayList<CoinWrapper<Dime>>();
    List<CoinWrapper<Dollar>> dollarWrapperContainer = new ArrayList<CoinWrapper<Dollar>>();
    List<CoinWrapper<HalfDollar>> halfDollarWrapperContainer = new ArrayList<CoinWrapper<HalfDollar>>();
    List<CoinWrapper<Nickel>> nickelWrapperContainer = new ArrayList<CoinWrapper<Nickel>>();
    List<CoinWrapper<Penny>> pennyWrapperContainer = new ArrayList<CoinWrapper<Penny>>();
    List<CoinWrapper<Quarter>> quarterWrapperContainer = new ArrayList<CoinWrapper<Quarter>>();

    public CoinProcessorImpl(BlockingQueue<Coin> hopper, Boolean controlFlag) {
        this.hopper = hopper;
        this.controlFlag = controlFlag;
    }

    private Boolean controlFlag;

    private BigDecimal totalValue;


    @Override
    public void run() {
        ScheduledFuture<Coin> futureCoin = null;
        totalValue = new BigDecimal("0");
        try {
            while(controlFlag) {
                Coin processingCoin = hopper.poll(15, TimeUnit.SECONDS);
                if(processingCoin!=null) {
                    totalValue = totalValue.add(new BigDecimal(processingCoin.getValue()+""));
                    System.out.println("    -Processing coin " + processingCoin.toString() + "; processing may take a while...\n");
                    if (processingCoin instanceof Dime) {
                        Thread.sleep(1000);
                        if(dimeWrapperContainer.get(dimeWrapperContainer.size()-1).isFull()){
                            dimeWrapperContainer.add(new CoinWrapper<Dime>(3, "Dime Wrapper"));
                        }
                        dimeWrapperContainer.get(dimeWrapperContainer.size()-1).addCoin((Dime) processingCoin);
                        System.out.println("    -Process of " + processingCoin.toString() + " took 1.00 second(s), total: $" + getTotalValue() + ", hopper count: " + hopper.size());
                    } else if (processingCoin instanceof Dollar) {
                        Thread.sleep(10000);
                        if(dollarWrapperContainer.get(dollarWrapperContainer.size()-1).isFull()){
                            dollarWrapperContainer.add(new CoinWrapper<Dollar>(4, "Dollar Wrapper"));
                        }
                        dollarWrapperContainer.get(dollarWrapperContainer.size()-1).addCoin((Dollar) processingCoin);
                        System.out.println("    -Process of " + processingCoin.toString() + " took 10.00 second(s), total: $" + getTotalValue() + ", hopper count: " + hopper.size());
                    } else if (processingCoin instanceof HalfDollar) {
                        Thread.sleep(5000);
                        if(halfDollarWrapperContainer.get(halfDollarWrapperContainer.size()-1).isFull()){
                            halfDollarWrapperContainer.add(new CoinWrapper<HalfDollar>(5, "HalfDollar Wrapper"));
                        }
                        halfDollarWrapperContainer.get(halfDollarWrapperContainer.size()-1).addCoin((HalfDollar) processingCoin);
                        System.out.println("    -Process of " + processingCoin.toString() + " took 5.00 second(s), total: $" + getTotalValue() + ", hopper count: " + hopper.size());
                    } else if (processingCoin instanceof Nickel) {
                        Thread.sleep(500);
                        if(nickelWrapperContainer.get(nickelWrapperContainer.size()-1).isFull()){
                            nickelWrapperContainer.add(new CoinWrapper<Nickel>(2, "Nickel Wrapper"));
                        }
                        nickelWrapperContainer.get(nickelWrapperContainer.size()-1).addCoin((Nickel) processingCoin);
                        System.out.println("    -Process of " + processingCoin.toString() + " took 0.50 second(s), total: $" + getTotalValue() + ", hopper count: " + hopper.size());
                    } else if (processingCoin instanceof Penny) {
                        Thread.sleep(100);
                        if(pennyWrapperContainer.get(pennyWrapperContainer.size()-1).isFull()){
                            pennyWrapperContainer.add(new CoinWrapper<Penny>(10, "Penny Wrapper"));
                        }
                        pennyWrapperContainer.get(pennyWrapperContainer.size()-1).addCoin((Penny) processingCoin);
                        System.out.println("    -Process of " + processingCoin.toString() + " took 0.10 second(s), total: $" + getTotalValue() + ", hopper count: " + hopper.size());
                    } else if (processingCoin instanceof Quarter) {
                        Thread.sleep(2500);
                        if(quarterWrapperContainer.get(quarterWrapperContainer.size()-1).isFull()){
                            quarterWrapperContainer.add(new CoinWrapper<Quarter>(6, "Quarter Wrapper"));
                        }
                        quarterWrapperContainer.get(quarterWrapperContainer.size()-1).addCoin((Quarter) processingCoin);
                        System.out.println("    -Process of " + processingCoin.toString() + " took 2.50 second(s), total: $" + getTotalValue() + ", hopper count: " + hopper.size());
                    }
                }else{
                    controlFlag = false;
                    System.out.println("No more coins on the hopper.");
                    printStats();
                }

            }

        } catch (InterruptedException e) {
            controlFlag = false;
            System.out.println("No more coins on the hopper.");
            printStats();
            //e.printStackTrace();
        }
    }



    @Override
    public void init() {
        dimeWrapperContainer.add(new CoinWrapper<Dime>(3, "Dime Wrapper"));
        dollarWrapperContainer.add(new CoinWrapper<Dollar>(4, "Dollar Wrapper"));
        halfDollarWrapperContainer.add(new CoinWrapper<HalfDollar>(5, "HalfDollar Wrapper"));
        nickelWrapperContainer.add(new CoinWrapper<Nickel>(2, "Nickel Wrapper"));
        pennyWrapperContainer.add(new CoinWrapper<Penny>(10, "Penny Wrapper"));
        quarterWrapperContainer.add(new CoinWrapper<Quarter>(6, "Quarter Wrapper"));
    }

    @Override
    public void clear() {
        dimeWrapperContainer.clear();
        dollarWrapperContainer.clear();
        halfDollarWrapperContainer.clear();
        nickelWrapperContainer.clear();
        pennyWrapperContainer.clear();
        quarterWrapperContainer.clear();
    }

    public void printStats(){
        //Total wrappers              : 10
        // - Dollar Wrappers          : 1
        // - Half-Dollar Wrappers     : 1
        // - Quarter Wrappers         : 1
        // - Dime Wrappers            : 2
        // - Nickel Wrappers          : 3
        // - Penny Wrappers           : 2
        //Total value                 : $4.13
        System.out.printf("\n\nTotal wrappers\t\t\t\t\t: %d\n", (dimeWrapperContainer.size() + dollarWrapperContainer.size() + halfDollarWrapperContainer.size() +
                nickelWrapperContainer.size() + pennyWrapperContainer.size() + quarterWrapperContainer.size()));
        System.out.printf(" - Dollar Wrappers\t\t\t: %d\n",dollarWrapperContainer.size());
        System.out.printf(" - Half-Dollar Wrappers\t\t\t: %d\n",halfDollarWrapperContainer.size());
        System.out.printf(" - Quarter Wrappers\t\t\t: %d\n",quarterWrapperContainer.size());
        System.out.printf(" - Dime Wrappers\t\t\t: %d\n",dimeWrapperContainer.size());
        System.out.printf(" - Nickel Wrappers\t\t\t: %d\n", nickelWrapperContainer.size());
        System.out.printf(" - Penny Wrappers\t\t\t: %d\n",pennyWrapperContainer.size());
        System.out.printf("Total value\t\t\t\t\t: %f\n",getTotalValue());
    }

    private BigDecimal getTotalValue() {
       /* Float totalSum = 0f;

        Float dollarTotal = calculateGenericTotal(dimeWrapperContainer);
        Float halfDollarTotal = calculateGenericTotal(halfDollarWrapperContainer);
        Float quarterTotal = calculateGenericTotal(quarterWrapperContainer);
        Float dimeTotal = calculateGenericTotal(dimeWrapperContainer);
        Float nickelTotal = calculateGenericTotal(nickelWrapperContainer);
        Float pennyTotal = calculateGenericTotal(pennyWrapperContainer);*/

        return totalValue;//dollarTotal+halfDollarTotal+quarterTotal+dimeTotal+nickelTotal+pennyTotal;
    }

}
