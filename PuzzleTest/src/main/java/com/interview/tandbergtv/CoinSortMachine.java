package com.interview.tandbergtv;

import com.interview.tandbergtv.coinsort.coins.Coin;

/**
 * This is the coin sort machine. Your job, if you choose to accept it, is
 * to implement the functions of a coin sort machine (input coins in a hopper
 * and it will sort them out in each denomination and wraps them). Just as with
 * a real coin sort machine, the hopper acts like a buffer - holding the coins
 * until it is ready for processing while the machine processes each coin one
 * at a time. What is different from this "virtual" coin sort machine is that
 * the more valuable the coin is, the more processing has to be done to make
 * sure that the coin is legitimate (you'll need to simulate this delay).
 * 
 * Processing delay values for each coin denomination:
 * 		- penny: 100ms
 * 		- nickel: 500ms
 * 		- dime: 1000ms
 * 		- quarter: 2500ms
 * 		- half-dollar: 5000ms
 * 		- dollar: 10000ms
 * 
 * Also as in real life, it would be possible to add coins to the hopper while
 * the machine is busy processing each coin (you must not block). 
 *
 * After a coin has been processed, it is inserted into coin wrappers. The number
 * of coins that can fit in a wrapper is determined by its denomination:
 * 		- pennies: 10		($0.10)
 * 		- nickels: 2		($0.10)
 * 		- dimes: 3			($0.30)
 * 		- quarters: 6		($1.50)
 * 		- half-dollars: 5	($2.50)
 * 		- dollar: 4			($4.00)
 * It is indeed strange that so few coins are allowed in each wrapper, but that
 * is what your bank requests.  
 * 
 * The coin sort machine should stop as soon as it encounters a damaged coin 
 * or if the hopper runs out of coins for 15 seconds. Once the machine stops,
 * it should display statistics of coins processed (see sample output in
 * the TestDriver - it should be like that).
 */
public interface CoinSortMachine {
	
	// number of coins that can fit on the "hopper"
	public static final int MAX_HOPPER_COUNT = 15;
	
	/**
	 * Starts up the coin sort machine
	 */
	void start();
	
	/**
	 * This should remove ALL coins from the machine; start from a clean
	 * state.
	 */
	void restart();

	/**
	 * Method to add a coin to the hopper. Use this to let people
	 * donate coins.
	 * @param coin Coin to add to the hopper
	 */
	void addCoinToHopper(Coin coin);
	
	/**
	 * Check to see if the hopper is full.
	 * @return true if the hopper is full; false otherwise
	 */
	boolean isHopperFull();
	
	/**
	 * Find out how many coins are currently on the hopper
	 * @return int number of coins currently on the hopper
	 */
	int currentHopperSize();
	
	/**
	 * Print to the console the total value that is in the wrappers as well
	 * as the nunber of wrappers that have coins in them
	 * Output should look something like this:
	 * 
	 * Total wrappers              : 9
	 *  - Dollar Wrappers          : 1
	 *  - Half-Dollar Wrappers     : 1
	 *  - Quarter Wrappers         : 1
	 *  - Dime Wrappers            : 2
	 *  - Nickel Wrappers          : 3
	 *  - Penny Wrappers           : 1
	 * Total value                 : $4.04
	 */
	void printStats();
}
