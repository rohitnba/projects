package com.interview.tandbergtv;

import com.interview.tandbergtv.coinsort.coins.*;

import java.util.concurrent.*;

public class CoinSortMachineImpl implements CoinSortMachine{

    BlockingQueue<Coin> hopper = new ArrayBlockingQueue<Coin>(MAX_HOPPER_COUNT);
    //ExecutorService executorService1 = Executors.newSingleThreadExecutor();
    CoinProcessor coinProcessor;



    @Override
    public void start() {
        System.out.println("Coin sorter has been started...");
        coinProcessor = new CoinProcessorImpl(hopper, true);
        coinProcessor.init();
        new Thread(coinProcessor).start();
    }

    @Override
    public void restart() {
        //Clear the hopper and the wrappers
        hopper.clear();
        coinProcessor.clear();
        //Start it back
        start();
    }

    @Override
    public void addCoinToHopper(Coin coin) {
        //Bill donates a [HalfDollar; S/N: 200; Damaged: false]   (hopper count: 1)
        hopper.add(coin);
        //coinProcessor.process();
        //executor.get

    }

    @Override
    public boolean isHopperFull() {
        return hopper.size()<=15?false:true;
    }

    @Override
    public int currentHopperSize() {
        return hopper.size();
    }

    @Override
    public void printStats() {

    }
}