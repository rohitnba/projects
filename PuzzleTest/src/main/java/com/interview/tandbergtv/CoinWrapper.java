package com.interview.tandbergtv;

import java.util.Stack;

import com.interview.tandbergtv.coinsort.coins.Coin;

/**
 * Implementation of a "coin wrapper". An instance of this class
 * would hold a list of coins of a certain denomination. Coins can only be
 * inserted and removed from the top of the wrapper (as in real-life).
 *
 * @param <T> The type of coin this wrapper would hold
 */
public class CoinWrapper<T extends Coin> {
	
	private Stack<T> coins;
	private int maxCoinCount;
	private String label;
	
	/**
	 * Constructor
	 * @param maxCoinCount Pass in the max coin count for the wrapper
	 */
	public CoinWrapper(int maxCoinCount, String label) {
		coins = new Stack<T>();
		this.maxCoinCount = maxCoinCount;
		this.label = label;
	}
	
	/**
	 * @param coin The coin to add to the wrapper. Does not add the coin if
	 * the wrapper is full.
	 */
	public void addCoin(T coin) {
		if (!isFull()) {
			coins.push(coin);
		}
	}
	
	/**
	 * Removes and returns the coin at the top. Returns null if the
	 * wrapper is empty.
	 * @return T the coin at the top of the wrapper; null if empty
	 */
	public T removeTopCoin() {
		if (coins.size() > 0) {
			return coins.pop();
		}
		return null;
	}
	
	/**
	 * Returns a reference to the coin at the top, but does not remove it
	 * from the wrapper
	 * @return T the coin at the top of the wrapper; null if empty
	 */
	public T lookAtTopCoin() {
		if (coins.size() > 0) {
			return coins.peek();
		}
		return null;
	}
	
	/**
	 * Check how many coins are in the wrapper 
	 * @return int the number of coins in the wrapper
	 */
	public int size() {
		return coins.size();
	}
	
	/**
	 * Check to see if the wrapper is full
	 * @return boolean true if wrapper is full; else false
	 */
	public boolean isFull() {
		return size() >= getMaxCoinCount();
	}
	
	/**
	 * @return The maximuim coin count that this wrapper can hold.
	 */
	public int getMaxCoinCount() {
		return maxCoinCount;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
