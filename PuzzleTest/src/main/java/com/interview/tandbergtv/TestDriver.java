package com.interview.tandbergtv;

import java.util.ArrayList;
import java.util.List;

import com.interview.tandbergtv.coinsort.coins.Coin;
import com.interview.tandbergtv.coinsort.coins.Dime;
import com.interview.tandbergtv.coinsort.coins.Dollar;
import com.interview.tandbergtv.coinsort.coins.HalfDollar;
import com.interview.tandbergtv.coinsort.coins.Nickel;
import com.interview.tandbergtv.coinsort.coins.Penny;
import com.interview.tandbergtv.coinsort.coins.Quarter;

/**
 * Test driver for your code
 */
public class TestDriver {

	public static void main(String args[]) {
		
		// construct the coin sort machine
		CoinSortMachine coinSortMachine = new CoinSortMachineImpl();
		
		// start up the coin sorter machine
		coinSortMachine.start();

		List<Coin> moneyBag1 = new ArrayList<Coin>();
		moneyBag1.add(new Penny(100, false));
		moneyBag1.add(new Dime(101, false));
		moneyBag1.add(new Dime(102, false));
		moneyBag1.add(new Nickel(103, false));
		moneyBag1.add(new Dime(104, false));
		moneyBag1.add(new Nickel(105, false));
		moneyBag1.add(new Dime(106, false));
		moneyBag1.add(new Penny(107, false));
		moneyBag1.add(new Penny(108, false));
		moneyBag1.add(new Dime(109, false));
		moneyBag1.add(new Nickel(110, false));
		moneyBag1.add(new Nickel(111, false));
		moneyBag1.add(new Penny(112, false));
		moneyBag1.add(new Penny(113, false));
		moneyBag1.add(new Penny(114, false));
		moneyBag1.add(new Nickel(115, false));
		moneyBag1.add(new Penny(116, false));
		moneyBag1.add(new Penny(117, false));
		moneyBag1.add(new Penny(118, false));
		moneyBag1.add(new Penny(119, false));
		moneyBag1.add(new Penny(120, false));
		moneyBag1.add(new Penny(121, false));
		moneyBag1.add(new Penny(122, false));
		
		List<Coin> moneyBag2 = new ArrayList<Coin>();
		moneyBag2.add(new HalfDollar(200, false));
		moneyBag2.add(new Dollar(201, false));
		moneyBag2.add(new Dollar(202, false));
		moneyBag2.add(new HalfDollar(203, false));
		moneyBag2.add(new Quarter(204, false));
		
		CoinDonator poor = new CoinDonator("Bob", moneyBag1, 2, coinSortMachine);
		CoinDonator rich = new CoinDonator("Bill", moneyBag2, 3, coinSortMachine);
		
		new Thread(rich).start();
		new Thread(poor).start();
	}
}

//Sample output:
//Coin sorter has been started...
//
//Bill donates a [HalfDollar; S/N: 200; Damaged: false]   (hopper count: 1)
//	 -Processing coin [HalfDollar; S/N: 200; Damaged: false]; processing may take a while...
//
//Bob donates a [Penny; S/N: 100; Damaged: false]   (hopper count: 1)
//Bob donates a [Dime; S/N: 101; Damaged: false]   (hopper count: 2)
//Bill donates a [Dollar; S/N: 201; Damaged: false]   (hopper count: 3)
//Bob donates a [Dime; S/N: 102; Damaged: false]   (hopper count: 4)
//	 -Process of [HalfDollar; S/N: 200; Damaged: false] took 5.00 second(s), total: $0.50, hopper count: 4
//	 -Processing coin [Penny; S/N: 100; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 100; Damaged: false] took 0.10 second(s), total: $0.51, hopper count: 3
//	 -Processing coin [Dime; S/N: 101; Damaged: false]; processing may take a while...
//
//Bill donates a [Dollar; S/N: 202; Damaged: false]   (hopper count: 3)
//Bob donates a [Nickel; S/N: 103; Damaged: false]   (hopper count: 4)
//	 -Process of [Dime; S/N: 101; Damaged: false] took 1.00 second(s), total: $0.61, hopper count: 4
//	 -Processing coin [Dollar; S/N: 201; Damaged: false]; processing may take a while...
//
//Bob donates a [Dime; S/N: 104; Damaged: false]   (hopper count: 4)
//Bill donates a [HalfDollar; S/N: 203; Damaged: false]   (hopper count: 5)
//Bob donates a [Nickel; S/N: 105; Damaged: false]   (hopper count: 6)
//Bill donates a [Quarter; S/N: 204; Damaged: false]   (hopper count: 7)
//Bob donates a [Dime; S/N: 106; Damaged: false]   (hopper count: 8)
//Bob donates a [Penny; S/N: 107; Damaged: false]   (hopper count: 9)
//***Donator 'Bill' has no more coins to give.
//Bob donates a [Penny; S/N: 108; Damaged: false]   (hopper count: 10)
//	 -Process of [Dollar; S/N: 201; Damaged: false] took 10.00 second(s), total: $1.61, hopper count: 10
//	 -Processing coin [Dime; S/N: 102; Damaged: false]; processing may take a while...
//
//	 -Process of [Dime; S/N: 102; Damaged: false] took 1.00 second(s), total: $1.71, hopper count: 9
//	 -Processing coin [Dollar; S/N: 202; Damaged: false]; processing may take a while...
//
//Bob donates a [Dime; S/N: 109; Damaged: false]   (hopper count: 9)
//Bob donates a [Nickel; S/N: 110; Damaged: false]   (hopper count: 10)
//Bob donates a [Nickel; S/N: 111; Damaged: false]   (hopper count: 11)
//Bob donates a [Penny; S/N: 112; Damaged: false]   (hopper count: 12)
//Bob donates a [Penny; S/N: 113; Damaged: false]   (hopper count: 13)
//	 -Process of [Dollar; S/N: 202; Damaged: false] took 10.00 second(s), total: $2.71, hopper count: 13
//	 -Processing coin [Nickel; S/N: 103; Damaged: false]; processing may take a while...
//
//	 -Process of [Nickel; S/N: 103; Damaged: false] took 0.50 second(s), total: $2.76, hopper count: 12
//	 -Processing coin [Dime; S/N: 104; Damaged: false]; processing may take a while...
//
//Bob donates a [Penny; S/N: 114; Damaged: false]   (hopper count: 12)
//	 -Process of [Dime; S/N: 104; Damaged: false] took 1.00 second(s), total: $2.86, hopper count: 12
//	 -Processing coin [HalfDollar; S/N: 203; Damaged: false]; processing may take a while...
//
//Bob donates a [Nickel; S/N: 115; Damaged: false]   (hopper count: 12)
//Bob donates a [Penny; S/N: 116; Damaged: false]   (hopper count: 13)
//	 -Process of [HalfDollar; S/N: 203; Damaged: false] took 5.00 second(s), total: $3.36, hopper count: 13
//	 -Processing coin [Nickel; S/N: 105; Damaged: false]; processing may take a while...
//
//Bob donates a [Penny; S/N: 117; Damaged: false]   (hopper count: 13)
//	 -Process of [Nickel; S/N: 105; Damaged: false] took 0.50 second(s), total: $3.41, hopper count: 13
//	 -Processing coin [Quarter; S/N: 204; Damaged: false]; processing may take a while...
//
//Bob donates a [Penny; S/N: 118; Damaged: false]   (hopper count: 13)
//	 -Process of [Quarter; S/N: 204; Damaged: false] took 2.50 second(s), total: $3.66, hopper count: 13
//	 -Processing coin [Dime; S/N: 106; Damaged: false]; processing may take a while...
//
//	 -Process of [Dime; S/N: 106; Damaged: false] took 1.00 second(s), total: $3.76, hopper count: 12
//	 -Processing coin [Penny; S/N: 107; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 107; Damaged: false] took 0.10 second(s), total: $3.77, hopper count: 11
//	 -Processing coin [Penny; S/N: 108; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 108; Damaged: false] took 0.10 second(s), total: $3.78, hopper count: 10
//	 -Processing coin [Dime; S/N: 109; Damaged: false]; processing may take a while...
//
//Bob donates a [Penny; S/N: 119; Damaged: false]   (hopper count: 10)
//	 -Process of [Dime; S/N: 109; Damaged: false] took 1.00 second(s), total: $3.88, hopper count: 10
//	 -Processing coin [Nickel; S/N: 110; Damaged: false]; processing may take a while...
//
//	 -Process of [Nickel; S/N: 110; Damaged: false] took 0.50 second(s), total: $3.93, hopper count: 9
//	 -Processing coin [Nickel; S/N: 111; Damaged: false]; processing may take a while...
//
//	 -Process of [Nickel; S/N: 111; Damaged: false] took 0.50 second(s), total: $3.98, hopper count: 8
//	 -Processing coin [Penny; S/N: 112; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 112; Damaged: false] took 0.10 second(s), total: $3.99, hopper count: 7
//	 -Processing coin [Penny; S/N: 113; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 113; Damaged: false] took 0.10 second(s), total: $4.00, hopper count: 6
//	 -Processing coin [Penny; S/N: 114; Damaged: false]; processing may take a while...
//
//Bob donates a [Penny; S/N: 120; Damaged: false]   (hopper count: 6)
//	 -Process of [Penny; S/N: 114; Damaged: false] took 0.10 second(s), total: $4.01, hopper count: 6
//	 -Processing coin [Nickel; S/N: 115; Damaged: false]; processing may take a while...
//
//	 -Process of [Nickel; S/N: 115; Damaged: false] took 0.50 second(s), total: $4.06, hopper count: 5
//	 -Processing coin [Penny; S/N: 116; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 116; Damaged: false] took 0.10 second(s), total: $4.07, hopper count: 4
//	 -Processing coin [Penny; S/N: 117; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 117; Damaged: false] took 0.10 second(s), total: $4.08, hopper count: 3
//	 -Processing coin [Penny; S/N: 118; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 118; Damaged: false] took 0.10 second(s), total: $4.09, hopper count: 2
//	 -Processing coin [Penny; S/N: 119; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 119; Damaged: false] took 0.10 second(s), total: $4.10, hopper count: 1
//	 -Processing coin [Penny; S/N: 120; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 120; Damaged: false] took 0.10 second(s), total: $4.11, hopper count: 0
//Bob donates a [Penny; S/N: 121; Damaged: false]   (hopper count: 1)
//	 -Processing coin [Penny; S/N: 121; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 121; Damaged: false] took 0.10 second(s), total: $4.12, hopper count: 0
//Bob donates a [Penny; S/N: 122; Damaged: false]   (hopper count: 1)
//	 -Processing coin [Penny; S/N: 122; Damaged: false]; processing may take a while...
//
//	 -Process of [Penny; S/N: 122; Damaged: false] took 0.10 second(s), total: $4.13, hopper count: 0
//***Donator 'Bob' has no more coins to give.
//
//No more coins on the hopper.
//
//Total wrappers              : 10
// - Dollar Wrappers          : 1
// - Half-Dollar Wrappers     : 1
// - Quarter Wrappers         : 1
// - Dime Wrappers            : 2
// - Nickel Wrappers          : 3
// - Penny Wrappers           : 2
//Total value                 : $4.13
//
//
//Total running time: 56.17 seconds
