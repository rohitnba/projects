package com.interview.tandbergtv.coinsort.coins;


/**
 * A coin. All coins should extend this class.
 */
public abstract class Coin {
	
	public Coin(long serialNumber, boolean damaged) {
		this.serialNumber = serialNumber;
		this.damaged = damaged;
	}
	
	private long serialNumber;
	private boolean damaged;
	
	public abstract float getValue();

	public boolean isDamaged() {
		return damaged;
	}

	public void setDamaged(boolean damaged) {
		this.damaged = damaged;
	}

	public long getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(long serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String toString() {
		return "[" + this.getClass().getSimpleName() + "; S/N: " + serialNumber + "; Damaged: " + damaged + "]";
	}
}
