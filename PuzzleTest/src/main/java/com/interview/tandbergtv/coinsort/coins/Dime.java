package com.interview.tandbergtv.coinsort.coins;


public class Dime extends Coin {
	public Dime(long serialNumber,boolean damaged) {
		super(serialNumber, damaged);
	}

	@Override
	public float getValue() {
		return 0.10f;
	}

}
