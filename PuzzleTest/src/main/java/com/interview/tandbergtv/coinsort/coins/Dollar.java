package com.interview.tandbergtv.coinsort.coins;


public class Dollar extends Coin{
	public Dollar(long serialNumber, boolean damaged) {
		super(serialNumber, damaged);
	}

	@Override
	public float getValue() {
		return 1.00f;
	}

}
