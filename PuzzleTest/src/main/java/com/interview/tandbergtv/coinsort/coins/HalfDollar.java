package com.interview.tandbergtv.coinsort.coins;

public class HalfDollar extends Coin {
	public HalfDollar(long serialNumber, boolean damaged) {
		super(serialNumber, damaged);
	}

	@Override
	public float getValue() {
		return 0.50f;
	}

}
