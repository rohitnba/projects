package com.interview.tandbergtv.coinsort.coins;

public class Nickel extends Coin {
	public Nickel(long serialNumber, boolean damaged) {
		super(serialNumber, damaged);
	}

	@Override
	public float getValue() {
		return 0.05f;
	}

}
