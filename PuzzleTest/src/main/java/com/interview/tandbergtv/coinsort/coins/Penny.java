package com.interview.tandbergtv.coinsort.coins;

public class Penny extends Coin {

	public Penny(long serialNumber, boolean damaged) {
		super(serialNumber, damaged);
	}

	@Override
	public float getValue() {
		return 0.01f;
	}

}
