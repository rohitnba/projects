package com.interview.tandbergtv.coinsort.coins;

public class Quarter extends Coin {
	public Quarter(long serialNumber, boolean damaged) {
		super(serialNumber, damaged);
	}

	@Override
	public float getValue() {
		return 0.25f;
	}

}
