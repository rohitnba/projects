package com.interview.tandbergtv.coinsort.exceptions;


/**
 * Exception that happens when a Jam occurs (when the machine encounters a 
 * damaged coin while processing it)
 */
public class CoinJamException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public CoinJamException() {
		super();
	}
	public CoinJamException(String s) {
		super(s);
	}
}
