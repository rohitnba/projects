package com.java2novice;

/**
 * Created by r0h17 on 2/1/15.
 */
public class BInaryToDecimal {

    public static void main(String[] args) {
        int binary = 10011011;

        int decimal = 0;
        int power = 0;
        while(binary>0){
            int temp = binary%10;
            binary/=10;
            decimal+=temp*Math.pow(2, power);
            power++;
        }
        System.out.println(decimal);
    }
}
