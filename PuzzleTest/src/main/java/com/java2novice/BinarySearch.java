package com.java2novice;

import com.mysamples.ArrayFindWordTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by r0h17 on 2/4/15.
 */
public class BinarySearch {
    public static void main(String[] args) {
        ArrayFindWordTest.printArray(mergeSort(new Integer[]{6, 4, 9, 5}));
        List<Integer> inputs = new ArrayList<Integer>(){{
            add(6);add(4);add(9);add(5);
        }};
        for(Integer input: quickSort(inputs)){
            System.out.println(input);
        }

        Integer i = 3;
        Test t1 = new Test(i);
        Test t2 = new Test(i);

        if(t1.equals(t2)) System.out.println("yes1");
        if(t1==t2) System.out.println("rest2");
        //if(t1==s) System.out.println("test2");

    }

    static class Test{
        Integer test;

        Test(Integer test) {
            this.test = test;
        }

        @Override
        public boolean equals(Object obj) {
            System.out.println("equals");
            return super.equals(obj);
        }
    }

    private static int doBinarySearch(Integer[] input, int indexOf, int low, int high) {
        int pivot = (low + high)/2;
        if(input[pivot] == indexOf) return pivot;
        else if(input[pivot]>indexOf) return doBinarySearch(input, indexOf, low, pivot-1);
        else if(input[pivot]<indexOf) return doBinarySearch(input, indexOf, pivot, high);
        else return -1;
    }

    private static Integer[] bubbleSort(Integer[] input){
        boolean swapHappened;
        do{
            swapHappened = false;
            for (int i = 0; i < input.length - 1; i++) {
                if(input[i+1]<input[i]){
                    input[i] = input[i] ^ input[i+1];
                    input[i+1] = input[i] ^ input[i+1];
                    input[i] = input[i] ^ input[i+1];
                    swapHappened = true;
                }
            }
        }while (swapHappened);
        return input;
    }

    private static List<Integer> quickSort(List<Integer> inputs){
        if (inputs.size() < 2) {
            return inputs;
        }

        Integer pivot = inputs.get(0);
        List<Integer> lowerArray = new ArrayList<>();
        List<Integer> higherArray = new ArrayList<>();
        List<Integer> sortedArray = new ArrayList<>();

        for (int i=1; i<inputs.size();i++){
            if(inputs.get(i)>pivot) higherArray.add(inputs.get(i));
            else lowerArray.add(inputs.get(i));
        }

        sortedArray.addAll(quickSort(lowerArray));
        sortedArray.add(pivot);
        sortedArray.addAll(quickSort(higherArray));
        return sortedArray;

    }

    private static Integer[] mergeSort(Integer[] inputs){

        if(inputs.length ==1) return inputs;

        int pivot = inputs.length/2;
        Integer[] left = new Integer[pivot],right = new Integer[inputs.length-pivot];

        left = mergeSort(Arrays.copyOfRange(inputs, 0 , pivot));
        right = mergeSort(Arrays.copyOfRange(inputs, pivot, inputs.length));

        return merge(left, right);

    }

    private static Integer[] merge(Integer[] left, Integer[] right) {
        Integer[] result = new Integer[left.length+right.length];
        int resultIndex = 0;
        while(left.length>0 || right.length>0){
            if(left.length>0 && right.length>0){
                if(left[0]<right[0]){
                    result[resultIndex] = left[0];
                    resultIndex++;
                    left = Arrays.copyOfRange(left, 1, left.length);
                }else{
                    result[resultIndex] = right[0];
                    resultIndex++;
                    right = Arrays.copyOfRange(right, 1, right.length);
                }
            }else if(left.length>0){
                result[resultIndex] = left[0];
                resultIndex++;
                left = Arrays.copyOfRange(left, 1, left.length);
            }else if(right.length>0){
                result[resultIndex] = right[0];
                resultIndex++;
                right = Arrays.copyOfRange(right, 1, right.length);
            }
        }
        return result;
    }
}
