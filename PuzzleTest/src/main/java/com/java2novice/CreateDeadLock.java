package com.java2novice;

/**
 * Created by r0h17 on 2/1/15.
 */
public class CreateDeadLock {

    String str1 = new String("Test1");
    String str2 = new String("test2");

    Thread firstThread = new Thread(){
        @Override
        public void run() {
            while(true) {
                synchronized (str1) {
                    synchronized (str2) {
                        System.out.println(str1 + str2);
                    }
                }
            }
        }
    };

    Thread secondThread = new Thread(){
        @Override
        public void run() {
            while(true) {
                synchronized (str2) {
                    synchronized (str1) {
                        System.out.println(str1 + str2);
                    }
                }
            }
        }
    };


    public static void main(String[] args) {

        CreateDeadLock createDeadLock = new CreateDeadLock();
        createDeadLock.firstThread.start();
        createDeadLock.secondThread.start();

    }
}
