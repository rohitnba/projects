package com.java2novice;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by r0h17 on 2/1/15.
 */
public class DecimalToBinary {

    public static void main(String[] args) {
        int decimal = 50;
        List<Integer> binaries = new ArrayList<>();
        while(decimal>0){
            binaries.add(decimal%2);
            decimal/=2;
        }

        int decimalOut = 0;
        int power = 0;

        for (int j = 0; j < binaries.size(); j++) {
            decimalOut+=binaries.get(j)==1?0:1*Math.pow(2, j);
        }
        System.out.println(decimalOut);

    }
}
