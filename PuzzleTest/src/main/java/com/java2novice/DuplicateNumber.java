package com.java2novice;

/**
 * Created by r0h17 on 2/1/15.
 */
public class DuplicateNumber {
    public static void main(String[] args) {
        Integer[] inputs = {1,2,3,4,5,6,7,8,9,8,10,11,12,13,14};

        Integer biggestNumber = 0;
        Integer sum  = 0;
        for (Integer n: inputs){
            if(n>biggestNumber) biggestNumber = n;
            sum+=n;
        }

        double actualSum = biggestNumber*(biggestNumber + 1)*0.5;

        System.out.println("Duplicate Number"+(actualSum - sum));
    }
}
