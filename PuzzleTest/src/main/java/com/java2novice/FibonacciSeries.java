package com.java2novice;

/**
 * Created by r0h17 on 2/1/15.
 */
public class FibonacciSeries {

    public static void main(String[] args) {
        System.out.println(printTillNumber(14));
        for (int i = 1; i < 15; i++) {

            System.out.println(printTillNumber(i));
        }
    }

    private static int printTillNumber(int i) {
        if(i==1 || i==2) return 1;
        return printTillNumber(i-1)+printTillNumber(i-2);
    }
}
