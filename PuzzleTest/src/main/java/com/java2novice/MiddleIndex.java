package com.java2novice;

/**
 * Created by r0h17 on 2/1/15.
 */
public class MiddleIndex {
    public static void main(String[] args) {
        Integer[] inputs = {2,4,4,4,5,1};

        int outerForSum = 0;
        for (int i = 0; i < inputs.length; i++) {
            outerForSum+=inputs[i];
            Integer innerSum = 0;
            for (int j = i+1; j < inputs.length; j++) {
                innerSum+=inputs[j];
            }
            if(outerForSum == innerSum){
                System.out.println("Starting from 0 to "+(i)+" we get the balanced sides");
                break;
            }
        }
    }
}
