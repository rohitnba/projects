package com.java2novice;

import java.util.Arrays;

/**
 * Created by r0h17 on 2/1/15.
 */
public class MyArrayList<T> {

    T[] objectArray;
    int initialSize = 0;

    public MyArrayList() {
        this.objectArray = (T[]) new Object[10];
    }

    public void add(T element){
        objectArray[initialSize] = element;
        initialSize++;
        if(objectArray.length-initialSize==0){
            objectArray = Arrays.copyOf(objectArray, objectArray.length*2);
        }
    }

    public static void main(String[] args) {
        MyArrayList<Integer> list = new MyArrayList<>();
        for (int i = 0; i < 12; i++) {
            list.add(i);
        }
        System.out.println("");
    }
}
