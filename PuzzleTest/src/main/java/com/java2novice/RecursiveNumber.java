package com.java2novice;

/**
 * Created by r0h17 on 2/1/15.
 */
public class RecursiveNumber {

    public static void main(String[] args) {
        recursiveNum(12345);
    }

    private static void recursiveNum(Integer input) {
        Integer output = 0;
        while(input > 0) {
            output = output*10 + (input % 10);
            input /= 10;

        }
        System.out.println(output);
    }
}
