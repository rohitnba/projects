package com.java2novice;

/**
 * Created by r0h17 on 2/1/15.
 */
public class RecursiveRerverse {
    public static void main(String[] args) {
        String input = "The fox jumps over the lazy dog";
        System.out.println(recursiveReverse(input));
    }

    private static String recursiveReverse(String input) {
        if(input.length()==1) return input;
        return input.charAt(input.length()-1)+recursiveReverse(input.substring(0, input.length()-1));
    }

    public static String testRecursive(int times){
        String output = "";
        times--;
        if(times !=0) output = testRecursive(times);
        return output+times;
    }
}
