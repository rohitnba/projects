package com.java2novice;

class SingletonClass{
        private static SingletonClass instance;

        private SingletonClass(){};

        public static SingletonClass getInstance(){
            if(instance == null){
                instance = new SingletonClass();
            }
            return instance;
        }

    public void testMe(){
        System.out.println("Hey.... it is working!!!");
    }
}