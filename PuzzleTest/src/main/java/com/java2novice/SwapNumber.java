package com.java2novice;

/**
 * Created by r0h17 on 2/1/15.
 */
public class SwapNumber {
    public static void main(String[] args) {
        traditionalSwap();

        mordenSwap();
    }

    private static void mordenSwap() {
        int num1 = 10, num2 = 20;
        num1 = num1 ^ num2;
        num2 = num1 ^ num2;
        num1 = num1 ^ num2;

        System.out.println(num1 + " : "+ num2);
    }

    private static void traditionalSwap() {
        int num1 = 10, num2 = 20;

        num1 = num1 + num2;
        num2 = num1 - num2;
        num1 = num1 - num2;

        System.out.println(num1 + " : " + num2
        );
    }
}
