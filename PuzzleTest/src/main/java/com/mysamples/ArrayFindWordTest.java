package com.mysamples;

import java.util.Arrays;

/**
 * Created by r0h17 on 12/1/14.
 */
public class ArrayFindWordTest {

    public static void main(String[] args) {
        String[] targs = {"Hello", "World", "jumping", "fox", "eating", "grape", "fucked"};
        //findCharacter(targs, 'o');
        Integer[] integerArray = {1,2,3,4,5,6,7,8,9,10,59,23,67,12};
        int maxValue = Integer.MIN_VALUE;
        Integer[] tempArr = mergeSort(integerArray);
        System.out.println(tempArr[tempArr.length-1]);
       // −2, 1, −3, 4, −1, 2, 1, −5, 4
        /*quickSort(integerArray, 0, integerArray.length-1);

        */

        //printArray(mergeSort(integerArray));
        //Integer[] aux = integerArray.clone();

        //System.out.println(maxContigousSubarray(integerArray));
        //printArray(integerArray);

    }

    public static void mergeSort(Object src[], Object dest[], int low, int high, int off){
        for (int i = 0; i < high; i++) {
            for (int j = i; j > low && ((Comparable)dest[j-1]).compareTo((Comparable)dest[j])>0; j--) {
                Object temp = dest[j-1];
                dest[j-1] = dest[j];
                dest[j] = temp;
            }
        }
    }

    public static void printArray(Integer[] inputArray){
        for(int i:inputArray){
            System.out.print(i);
            System.out.print(" ");
        }
        System.out.println("");
    }

    public static int maxContigousSubarray(Integer[] input){
        int maxSum = input[0];

        //for (int i = 0; i < input.length; i++) {
            int currentSum = input[0];
            for (int j = 1; j < input.length; j++) {
                currentSum+=input[j];
                if(currentSum>maxSum)
                    maxSum = currentSum;
            }
        //}
        return maxSum;
    }

    private static Integer[] mergeSort(Integer[] inputArray){
        if(inputArray.length<=1) return inputArray;

        int middle = inputArray.length/2;
        Integer[] left = new Integer[middle], right = new Integer[inputArray.length-middle];

        left = mergeSort(Arrays.copyOfRange(inputArray, 0, middle));
        right = mergeSort(Arrays.copyOfRange(inputArray, middle, inputArray.length));

        return merge(left, right);
    }

    private static Integer[] merge(Integer[] left, Integer[] right) {
        Integer[] result = new Integer[left.length+right.length];
        int resultIndex = 0;
        while(left.length>0 || right.length>0){
            if(left.length>0 && right.length>0){
                if(left[0]<=right[0]){
                    result[resultIndex] = left[0];
                    resultIndex++;
                    left = Arrays.copyOfRange(left, 1, left.length);
                }else{
                    result[resultIndex] = right[0];
                    resultIndex++;
                    right = Arrays.copyOfRange(right, 1, right.length);
                }
            }else if(left.length>0){
                result[resultIndex] = left[0];
                resultIndex++;
                left = Arrays.copyOfRange(left, 1, left.length);
            }else if(right.length>0){
                result[resultIndex] = right[0];
                resultIndex++;
                right = Arrays.copyOfRange(right, 1, right.length);
            }
        }
        return result;
    }

    private static void quickSort(Integer[] integerArray, int lowerIndex, int higherIndex) {

        int i = lowerIndex;
        int j = higherIndex;
        // calculate pivot number, I am taking pivot as middle index number
        int pivot = integerArray[lowerIndex+(higherIndex-lowerIndex)/2];
        // Divide into two arrays
        while (i <= j) {
            /**
             * In each iteration, we will identify a number from left side which
             * is greater then the pivot value, and also we will identify a number
             * from right side which is less then the pivot value. Once the search
             * is done, then we exchange both numbers.
             */
            while (integerArray[i] < pivot) {
                i++;
            }
            while (integerArray[j] > pivot) {
                j--;
            }
            if (i <= j) {
                exchangeNumbers(integerArray, i, j);
                //move index to next position on both sides
                i++;
                j--;
            }
        }
        // call quickSort() method recursively
        if (lowerIndex < j)
            quickSort(integerArray, lowerIndex, j);
        if (i < higherIndex)
            quickSort(integerArray, i, higherIndex);
    }
    private static void exchangeNumbers(Integer[] integerArray, int i, int j) {
        int temp = integerArray[i];
        integerArray[i] = integerArray[j];
        integerArray[j] = temp;
    }


    private static void findCharacter(String[] targs, Character c) {
        for(String word: targs){
            if(word.contains(""+c)){
                System.out.println("word = " + word);
            }
        }
    }
}
