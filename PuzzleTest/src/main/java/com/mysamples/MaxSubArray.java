package com.mysamples;

/**
 * Created by r0h17 on 12/22/14.
 */
public class MaxSubArray {

    public static void main(String[] args) {
        int[] integerArray = {-2,1,-3,4,-1,2,-5,1,4};
        System.out.println(maxSubArray(integerArray));
        System.out.println(maxSubArray2(integerArray));
    }

    public static int maxSubArray(int[] A) {
        int sum = 0;
        int maxSum = Integer.MIN_VALUE;

        for (int i = 0; i < A.length; i++) {
            sum += A[i];
            maxSum = Math.max(maxSum, sum);

            if (sum < 0)
                sum = 0;
        }

        return maxSum;
    }
    public static int maxSubArray2(int[] A) {
        int newsum=A[0];
        int max=A[0];
        for(int i=1;i<A.length;i++){
            newsum=Math.max(newsum+A[i],A[i]);
            max= Math.max(max, newsum);
        }
        return max;
    }
}
