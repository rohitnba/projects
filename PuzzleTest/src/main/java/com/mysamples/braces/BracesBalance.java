package com.mysamples.braces;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by r0h17 on 1/5/15.
 */
public class BracesBalance {

    public static Map<Character, Character> bracesMap = new HashMap<Character, Character>(){{
       put('{','}');
       put('[',']');
       put('(',')');
    }};
    public static void main(String[] args) {
        String[] inputs = {"(a[b{c)d]e}", "}])", "({}","([Hell{} T(h(e[r]e))]boom)"};

        for(String input: inputs){
            char[] chs = input.toCharArray();
            ArrayDeque<Character> characterQueue = new ArrayDeque<Character>();
            for(char c: chs){
                if(bracesMap.containsKey(c)){
                    characterQueue.add(bracesMap.get(c));
                }else{
                    if(!characterQueue.isEmpty() && characterQueue.peekLast().compareTo(c) == 0){
                        characterQueue.pollLast();
                    }
                    else if(bracesMap.values().contains(c)){
                        System.out.println("fucked string" + new String(chs));
                        break;
                    }
                }
                //System.out.println(characterQueue);
            }
        }
    }
}
