package com.mysamples.dealer;

import java.io.FileReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by r0h17 on 1/7/15.
 */
public class KeyCounts {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(new FileReader("resources/keycountinput.txt"));
        PrintWriter pw = new PrintWriter(System.out);

        new KeyCounts().solve(sc, pw);

        pw.flush();
        pw.close();
        sc.close();
    }

    public void solve(Scanner sc, PrintWriter pw) {

            Map<String, Integer> keyCounts = new HashMap<String, Integer>();
            while (sc.hasNext()){
                //System.out.println(inputScaner.nextLine());
                String[] inputString = sc.nextLine().split(",");
                if(keyCounts.containsKey(inputString[0])){
                    keyCounts.put(inputString[0],keyCounts.get(inputString[0])+Integer.parseInt(inputString[1]));
                }else{
                    keyCounts.put(inputString[0], Integer.parseInt(inputString[1]));
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            for(String keys: keyCounts.keySet()){
                stringBuilder.append("This total for "+keys+" is "+keyCounts.get(keys)+". ");
            }
            pw.write(stringBuilder.toString());
    }
}
