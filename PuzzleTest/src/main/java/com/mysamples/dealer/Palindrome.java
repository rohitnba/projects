package com.mysamples.dealer;

import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by r0h17 on 1/7/15.
 */
public class Palindrome {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(new FileReader("resources/palindrome.txt"));
        PrintWriter pw = new PrintWriter(System.out);

        new Palindrome().solve(sc, pw);

        pw.flush();
        pw.close();
        sc.close();
    }

    public void solve(Scanner sc, PrintWriter pw) {

        while (sc.hasNext()){
            String inputString = sc.nextLine().replaceAll("[^a-zA-Z0-9]", "");
            pw.println(isPalindrome(inputString).toString());
            //System.out.println(updated.toString());
        }
    }

    public Boolean isPalindrome(String inputString){
        StringBuilder reverseString = new StringBuilder(inputString);
        return reverseString.reverse().toString().toLowerCase().equalsIgnoreCase(inputString.toLowerCase());
    }
}
