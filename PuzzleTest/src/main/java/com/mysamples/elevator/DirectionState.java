package com.mysamples.elevator;

/**
 * Created by r0h17 on 12/21/14.
 */
public class DirectionState {

    public enum Direction{
        UP, DOWN, STOPPED;
    }

    Direction direction;

    public DirectionState(Direction direction){
        this.direction = direction;
    };

    public void printMovingDirection(){
        switch (direction){
            case UP:
                System.out.println("Going Up");
                break;
            case DOWN:
                System.out.println("Going DOwn");
                break;
            case STOPPED:
                System.out.println("Not Moving");
                break;
        }
    }
}
