package com.mysamples.elevator;

/**
 * Created by r0h17 on 12/21/14.
 */
public class DoorState {

    public enum Door{
        OPEN, CLOSED;
    }

    Door door;

    public DoorState(Door door){
        this.door = door;
    };

    public void doorStatus(){
        switch (door){
            case OPEN:
                System.out.println("Door Open");
                break;
            case CLOSED:
                System.out.print("Door Closed");
                break;
        }
    }

}
