package com.mysamples.elevator;

import java.util.Set;

/**
 * Created by r0h17 on 12/21/14.
 */
public class Elevator {

    DirectionState.Direction direction;
    DoorState.Door door;
    Set<FloorData> floorDatas;

    public DirectionState.Direction getDirection() {
        return direction;
    }

    public void setDirection(DirectionState.Direction direction) {
        this.direction = direction;
    }

    public DoorState.Door getDoor() {
        return door;
    }

    public void setDoor(DoorState.Door door) {
        this.door = door;
    }

    public Set<FloorData> getFloorDatas() {
        return floorDatas;
    }

    public void setFloorDatas(Set<FloorData> floorDatas) {
        this.floorDatas = floorDatas;
    }
}
