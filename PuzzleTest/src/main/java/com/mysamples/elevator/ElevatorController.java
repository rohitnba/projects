package com.mysamples.elevator;

import java.util.*;

/**
 * Created by r0h17 on 12/21/14.
 */
public class ElevatorController {

    public ElevatorController(Elevator elevator) {
        this.elevator = elevator;
    }

    private Elevator elevator;

    public Elevator getElevator() {
        return elevator;
    }

    public void setElevator(Elevator elevator) {
        this.elevator = elevator;
    }

    public boolean addFloor(FloorData floorData){
        return elevator.getFloorDatas().add(floorData);
    }

    public FloorData getNextFloorData(){
        List<FloorData> floorDatas = new ArrayList<FloorData>();
        //Collections.copy(elevator.getFloorDatas().l, floorDatas);
        NextFloorComparator nextFloorComparator = new NextFloorComparator();
        Collections.sort(floorDatas, nextFloorComparator);

        return null;
    }
}
