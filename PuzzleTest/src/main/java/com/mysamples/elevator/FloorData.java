package com.mysamples.elevator;

/**
 * Created by r0h17 on 12/21/14.
 */
public class FloorData {
    private int floorNum;
    private DirectionState.Direction direction;

    public FloorData(int floorNum, DirectionState.Direction direction) {
        this.floorNum = floorNum;
        this.direction = direction;
    }

    public int getFloorNum() {
        return floorNum;
    }

    public void setFloorNum(int floorNum) {
        this.floorNum = floorNum;
    }

    public DirectionState.Direction getDirection() {
        return direction;
    }

    public void setDirection(DirectionState.Direction direction) {
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FloorData floorData = (FloorData) o;

        if (floorNum != floorData.floorNum && direction != floorData.direction) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = floorNum;
        result = 31 * result + direction.hashCode();
        return result;
    }
}
