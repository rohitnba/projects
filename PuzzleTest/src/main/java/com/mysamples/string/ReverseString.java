package com.mysamples.string;

/**
 * Created by r0h17 on 1/5/15.
 */
public class ReverseString {
    public static void main(String[] args) {
        String inputString = "The fox jumps over the lazy dog";

        String[] splitS = inputString.split(" ");
        for (int i = splitS.length-1; i >=0 ; --i) {
            System.out.printf(splitS[i]+" ");
        }
    }
}
